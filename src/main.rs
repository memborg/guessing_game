extern crate rand;

use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {

    let max = 101;
    println!("Gæt tallet mellem 1 og {}!", max-1);
    let secret_number = rand::thread_rng().gen_range(1, max);

    // println!("The secret number is: {}", secret_number);

    loop {
        println!("Kom med dit gæt");

        let mut guess = String::new();

        io::stdin().read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue
        };

        println!("Dit gæt: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("For lidt"),
            Ordering::Greater => println!("For meget"),
            Ordering::Equal => {
                println!("DU VANDT!");
                break;
            }
        }
    }
}
